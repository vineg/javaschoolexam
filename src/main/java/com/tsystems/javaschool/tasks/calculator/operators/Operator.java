package com.tsystems.javaschool.tasks.calculator.operators;

import com.tsystems.javaschool.tasks.calculator.Statement;

/**
 * Created by Vineg on 26-Apr-17.
 */
public abstract class Operator extends Statement{
    public Statement rightPart;
    public Statement leftPart;

    public Operator(Statement leftPart, Statement rightPart) {
        if(rightPart==null||leftPart==null){
            throw new IllegalArgumentException();
        }
        this.leftPart=leftPart;
        this.rightPart=rightPart;
    }
}
