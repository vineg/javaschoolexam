package com.tsystems.javaschool.tasks.calculator.operators;

import com.tsystems.javaschool.tasks.calculator.Statement;

/**
 * Created by Vineg on 26-Apr-17.
 */
public class Divide extends Operator {

    public Divide(Statement leftPart, Statement rightPart) {
        super(leftPart,rightPart);
    }

    @Override
    public float calculate() {
        float denominator = rightPart.calculate();
        if(denominator==0){
            throw new ArithmeticException();
        }
        return leftPart.calculate()/ denominator;
    }
}
