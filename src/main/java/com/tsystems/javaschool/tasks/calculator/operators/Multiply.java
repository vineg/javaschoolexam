package com.tsystems.javaschool.tasks.calculator.operators;

import com.tsystems.javaschool.tasks.calculator.Statement;

/**
 * Created by Vineg on 26-Apr-17.
 */
public class Multiply extends Operator {

    public Multiply(Statement leftPart, Statement rightPart) {
        super(leftPart, rightPart);
    }

    @Override
    public float calculate() {
        return leftPart.calculate()*rightPart.calculate();
    }
}
