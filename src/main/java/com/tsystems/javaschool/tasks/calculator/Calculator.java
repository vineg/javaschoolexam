package com.tsystems.javaschool.tasks.calculator;

import com.tsystems.javaschool.tasks.calculator.operators.*;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null) {
            return null;
        }
        statement = statement.replace(" ", "");
        Statement parsedStatement;
        try {
            parsedStatement = parse(statement);
        } catch (IllegalArgumentException e) {
            return null;
        }
        if (parsedStatement == null) {
            return null;
        }
        float result;
        try {
            result = parsedStatement.calculate();
        } catch (ArithmeticException e) {
            return null;
        }
        if (result % 1 == 0) {
            return String.valueOf((int) result);
        }
        return String.valueOf(Math.round(result * 10000) / 10000f);
    }

    private Statement parse(String str) {
        return parse(str, 0, str.length() - 1);
    }

    private Statement parse(String str, int startIdx, int endIdx) {
        int idx = endIdx;
        if (endIdx < 0) {
            return null;
        }
        char lastCharacter = str.charAt(idx);
        Statement rightPart;
        if (Character.isDigit(lastCharacter)) {
            char currentChar = str.charAt(idx);
            while (Character.isDigit(currentChar) || currentChar == '.') {
                idx--;
                if (idx < 0) {
                    break;
                }
                currentChar = str.charAt(idx);
            }
            rightPart = new NumberStatement(Float.parseFloat(str.substring(idx + 1, endIdx + 1)));
        } else if (lastCharacter == ')') {
            int expressionStart = endIdx;
            int depth = 1;
            while (depth != 0) {
                expressionStart--;
                if (str.charAt(expressionStart) == ')') {
                    depth++;
                } else if (str.charAt(expressionStart) == '(') {
                    depth--;
                }
            }
            idx = expressionStart - 1;
            rightPart = new Parenthesis(parse(str.substring(expressionStart + 1, endIdx)));
        } else {
            return null;
        }
        if (idx < startIdx) {
            return rightPart;
        } else {
            char operatorChar = str.charAt(idx);
            Statement leftPart = parse(str.substring(startIdx, idx));
            switch (operatorChar) {
                case '+':
                    return new Add(leftPart, rightPart);
                case '-':
                    return new Substract(leftPart, rightPart);
                case '*':
                    if (leftPart instanceof SecondOrderOperator) {
                        SecondOrderOperator leftPart2 = (SecondOrderOperator) leftPart;
                        Statement currentElement = leftPart2;
                        SecondOrderOperator parent = leftPart2;
                        while (currentElement instanceof SecondOrderOperator) {
                            parent = (SecondOrderOperator) currentElement;
                            currentElement = ((SecondOrderOperator) currentElement).rightPart;
                        }
                        parent.rightPart = new Multiply(parent.rightPart, rightPart);
                        return leftPart2;
                    } else {
                        return new Multiply(leftPart, rightPart);
                    }
                case '/':
                    if (leftPart instanceof SecondOrderOperator) {
                        SecondOrderOperator leftPart2 = (SecondOrderOperator) leftPart;
                        Statement currentElement = leftPart2;
                        SecondOrderOperator parent = leftPart2;
                        while (currentElement instanceof SecondOrderOperator) {
                            parent = (SecondOrderOperator) currentElement;
                            currentElement = ((SecondOrderOperator) currentElement).rightPart;
                        }
                        parent.rightPart = new Divide(parent.rightPart, rightPart);
                        return leftPart2;
                    } else {
                        return new Divide(leftPart, rightPart);
                    }
                default:
                    return null;
            }
        }
    }

}
