package com.tsystems.javaschool.tasks.calculator;

/**
 * Created by Vineg on 26-Apr-17.
 */
public class Parenthesis extends Statement {

    public Statement content;

    public Parenthesis(Statement content){
        this.content=content;
    }

    @Override
    public float calculate() {
        return content.calculate();
    }
}
