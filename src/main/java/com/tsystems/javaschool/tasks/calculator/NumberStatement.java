package com.tsystems.javaschool.tasks.calculator;

/**
 * Created by Vineg on 26-Apr-17.
 */
public class NumberStatement extends Statement {

    private final float value;

    public NumberStatement(float num){
        this.value=num;
    }

    @Override
    public float calculate() {
        return value;
    }
}
