package com.tsystems.javaschool.tasks.calculator.operators;

import com.tsystems.javaschool.tasks.calculator.Statement;

/**
 * Created by Vineg on 26-Apr-17.
 */
public abstract class SecondOrderOperator extends Operator {
    public SecondOrderOperator(Statement leftPart, Statement rightPart) {
        super(leftPart, rightPart);
    }
}
