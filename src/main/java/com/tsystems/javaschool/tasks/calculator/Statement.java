package com.tsystems.javaschool.tasks.calculator;

/**
 * Created by Vineg on 26-Apr-17.
 */
public abstract class Statement {
    public abstract float calculate();
}
