package com.tsystems.javaschool.tasks.duplicates;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.TreeMap;

public class DuplicateFinder {

    /**
     * Processes the specified file and puts into another sorted and unique
     * lines each followed by number of occurrences.
     *
     * @param sourceFile file to be processed
     * @param targetFile output file; append if file exist, create if not.
     * @return <code>false</code> if there were any errors, otherwise
     * <code>true</code>
     */
    public boolean process(File sourceFile, File targetFile) {
        if(sourceFile==null){
            throw new IllegalArgumentException();
        }
        if(targetFile==null){
            throw new IllegalArgumentException();
        }
        Scanner in;
        PrintWriter out;
        try {
            out = new PrintWriter(targetFile);
        } catch (FileNotFoundException e) {
            try {
                targetFile.createNewFile();
            } catch (IOException e1) {
                return false;
            }
            try {
                out = new PrintWriter(targetFile);
            } catch (FileNotFoundException e1) {
                return false;
            }
        }
        try {
            in = new Scanner(sourceFile);
        } catch (FileNotFoundException e) {
            return false;
        }
        TreeMap<String,Integer> map = new TreeMap<>();
        while (in.hasNext()) {
            String line = in.nextLine();
            if (!map.containsKey(line)) {
                map.put(line,1);
            }else{
                map.put(line,map.get(line)+1);
            }
        }
        for(String line:map.keySet()){
            out.println(line+"["+map.get(line)+"]");
        }
        out.close();
        return true;

    }


}
