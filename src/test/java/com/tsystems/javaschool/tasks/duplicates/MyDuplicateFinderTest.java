package com.tsystems.javaschool.tasks.duplicates;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class MyDuplicateFinderTest {

    private DuplicateFinder duplicateFinder = new DuplicateFinder();

    @Test
    public void test() throws IOException {
        File sourceFile = new File("dublicates1.txt");
        File targetFile = new File("answer1.txt");
        File testFile = new File("correctAnswer1.txt");
        targetFile.delete();
        duplicateFinder.process(sourceFile, targetFile);
        Assert.assertArrayEquals(Files.readAllBytes(targetFile.toPath()), Files.readAllBytes(testFile.toPath()));
        targetFile.delete();
    }

    @Test
    public void test1() {
        File sourceFile = new File("dublicates0.txt");
        File targetFile = new File("answer1.txt");
        Assert.assertFalse(duplicateFinder.process(sourceFile, targetFile));
    }


}