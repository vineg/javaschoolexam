package com.tsystems.javaschool.tasks.calculator;

import org.junit.Assert;
import org.junit.Test;

public class MyCalculatorTest {

    private Calculator calc = new Calculator();

    @Test
    public void evaluate() {
        //given
        String input = "4 * ( 6 - 2 * (4 - 2))";
        String expectedResult = "8";

        //run
        String result = calc.evaluate(input);

        //assert
        Assert.assertEquals(expectedResult, result);
    }
    @Test
    public void evaluate1() {
        //given
        String input = "(6/3/2+5*6/(13/2+4))/2+4+2+3/4-((12-3)*4+23*5/(6-3))";
        String expectedResult = "-65.6548";

        //run
        String result = calc.evaluate(input);

        //assert
        Assert.assertEquals(expectedResult, result);
    }
    @Test
    public void evaluate2() {
        //given
        String input = "1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1";
        String expectedResult = "41";

        //run
        String result = calc.evaluate(input);

        //assert
        Assert.assertEquals(expectedResult, result);
    }


}